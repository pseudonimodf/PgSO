#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <signal.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>


#define PORTNUMBER 54321
#define TIMEOUTCONN 5

#define SIZESTRING 50
#define SUCCESS 0
#define FAILURE -1
#define GETMAPPA 1
#define SOLDOUT -2
#define NOTPRESENT -11
#define GETBOOKING 12
#define DISDETTA 2
#define NOCOD -10
#define EXIT -3
#define SERVER_EXIT -4
#define POSTO_LIBERO 0
#define POSTO_OCCUPATO -1


int menu();

void manageSIGHUP(){
	puts("Hangup - terminale scollegato\n");
	exit(EXIT_SUCCESS);
}

void manageSIGINT(){
	puts("Interrupt\n");
	exit(EXIT_SUCCESS);
}

void manageSIGQUIT(){
	puts("Quit\n");
	exit(EXIT_SUCCESS);
}

void manageSIGILL(){
	fprintf(stderr, "Illegal instruction\n");
	exit(EXIT_FAILURE);
}

void manageSIGSEGV(){
	fprintf(stderr, "Segmentation violation\n");
	exit(EXIT_FAILURE);
}

void manageSIGALRM(){
	fprintf(stderr, "Timeout di connessione\n");
	exit(EXIT_FAILURE);
}

void manageSIGTERM(){
	puts("Termination\n");
	exit(EXIT_SUCCESS);
}


int main(){

	char nomecinema[SIZESTRING];
	int nfile;
	int npoltrone;
	int codIO;
	int postiliberi;
	int daprenotare;
	int fila;
	int poltrona;
	int i = 0;  // variabili ausiliarie
	int ii = 0;
	int x = 0;
	int y = 0;
	int inserito = -1;
	int coddisdetta;

	struct sockaddr_in servaddr;
	int sizeservaddr = sizeof(servaddr);
	int datasock;

	signal(SIGHUP, manageSIGHUP);
	signal(SIGINT, manageSIGINT);
	signal(SIGQUIT, manageSIGQUIT);
	signal(SIGILL, manageSIGILL);
	signal(SIGSEGV, manageSIGSEGV);
	signal(SIGALRM, manageSIGALRM);
	signal(SIGTERM, manageSIGTERM);

	// connection setting
	bzero(&servaddr, sizeservaddr);
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORTNUMBER);
	//servaddr.sin_addr.s_addr = htonl(INADDR_ANY);  // 0.0.0.0 per testing
	inet_aton("79.32.216.175", &servaddr.sin_addr);

	puts("Server occupato o offline");

	if((datasock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		fprintf(stderr, "Errore chiamata socket()\n");
		exit(EXIT_FAILURE);
	}

	alarm(TIMEOUTCONN);
	while((connect(datasock, (struct sockaddr *)&servaddr, sizeservaddr)) == -1 ) {
		if (errno != EINTR){
			fprintf(stderr, "Errore chiamata connect()\n");
			exit(EXIT_FAILURE);
		}
	}
	signal(SIGALRM, SIG_IGN);

	bzero(nomecinema, SIZESTRING);
	if (read(datasock, nomecinema, SIZESTRING) < 0) {
		fprintf(stderr, "Errore lettura nome cinema\n");
		exit(EXIT_FAILURE);
	}

	bzero(&nfile, 4);
	if (read(datasock, &nfile, 4) < 0) {
		fprintf(stderr, "Errore lettura numero file\n");
		exit(EXIT_FAILURE);
	}

	bzero(&npoltrone, 4);
	if (read(datasock, &npoltrone, 4) < 0) {
		fprintf(stderr, "Errore lettura numero poltrone\n");
		exit(EXIT_FAILURE);
	}


	printf("Benvenuto nel sistema di prenotazione ON-LINE del cinema \"%s\"\n", nomecinema);
	printf("Il cinema dispone di %d posti a sedere\n", (nfile * npoltrone));

	int mappa[nfile][npoltrone];  // <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< <<< 

	system("clear");
	
	do {
	
	codIO = menu();  // pu� ritornare con GETMAPPA o DISDETTA o EXIT

	switch(codIO){

		case EXIT: {  // invio al server EXIT
			if (write(datasock, &codIO, 4) < 0) {
				fprintf(stderr, "Errore invio EXIT\n");
				exit(EXIT_FAILURE);
			}
			break;
		}


		case GETMAPPA: {

			// invio richiesta GETMAPPA al server
			if (write(datasock, &codIO, 4) < 0) {  
				fprintf(stderr, "Errore invio GETMAPPA\n");
				exit(EXIT_FAILURE);
			}

			// risposta del server = FAILURE O SOLDOUT o numero posti liberi + mappa
			bzero(&codIO, 4);
			if((read(datasock, &codIO, 4) ) < 0){
				fprintf(stderr, "Errore risposta da parte del server\n");
				exit(EXIT_FAILURE);
			}

			switch (codIO){
				case FAILURE:
					puts("Errore da parte del server");
					exit(EXIT_SUCCESS);

				case SOLDOUT:
					puts("Spiacente, non vi sono piu' posti liberi!");
					puts("Provi in un secondo momento.");
					exit(EXIT_SUCCESS);

				default:
					postiliberi = codIO;  // assegna il numero di posti liberi
					break;
			}

			// lettura mappa del cinema
			bzero(mappa, sizeof(mappa));
			if((read(datasock, mappa, sizeof(mappa)) ) < 0){
				fprintf(stderr, "Errore lettura poltrone\n");
				exit(EXIT_FAILURE);
			}

			// stampa mappa cinema
			system("clear");
			printf("Sono rimasti %d posti liberi\n", postiliberi);
			puts("--- Mappa poltrone ---");
			printf("Poltrona n. ");
			for (i = 0; i < npoltrone; i++){
				if ((i + 1) < 10){
					printf(" %d ", i+1);
				} else {
					printf("%d ", i+1);
				}	
			}
			printf("\n");
			for (x = 0; x < nfile; x++){
				if ((x+1) < 10){
					printf("  Fila n. %d ", x+1);
				} else {
					printf("  Fila n.%d ", x+1);
				}
				for (y = 0; y < npoltrone; y++){
					if (mappa[x][y] == POSTO_LIBERO){
						printf("[ ]");
					}
					if (mappa[x][y] == POSTO_OCCUPATO){
						printf("[x]");
					}
				}
				printf("\n");
			}
			puts(" ----------------------- ");
			puts("| [x] poltrona occupata |");
			puts("| [ ] poltrona libera   |");
			puts(" ----------------------- ");

			//printf("Quantit� di file %d\n", nfile);
			//printf("Quantit� di poltrone %d\n", npoltrone);

			puts("---------------------------------------------------------");
			puts(" Digitare il numero di poltrone che si intende prenotare");
			puts(" [0] per tornare al menu");
			puts("---------------------------------------------------------");
			
			scanf("%d", &daprenotare);

			if (daprenotare == 0){  // torna al menu principale
				system("clear");
				break;
			}	

			if (daprenotare > postiliberi){  // invio al server EXIT

				codIO = EXIT;
				if (write(datasock, &codIO, 4) < 0) {
					fprintf(stderr, "Errore invio EXIT\n");
					exit(EXIT_FAILURE);
				}
				puts("Spiacente, non ci sono posti a sufficienza.");
				puts("Provi in un secondo momento.");
				puts("A presto...");
				exit(EXIT_SUCCESS);
			}


			if ((daprenotare > 0) && (daprenotare <= postiliberi)){

				int booking[daprenotare][2];  // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
				for (i = 0; i < daprenotare; i++){  // inizializza la mappa delle poltrone da prenotare a -1
					booking[i][0] = -1;             // (per non confonderla con la poltrona 00)
					booking[i][1] = -1;
				}


				for (i = 0; i < daprenotare; i++){
					fila = 0;
					poltrona = 0;
					puts("Inserire poltrona digitanto: n.FILA + spazio + n.POLTRONA");
					scanf("%d %d", &fila, &poltrona);
					fila--;
					poltrona--;



					if((fila < 0) || (fila > (nfile-1)) || (poltrona < 0) || (poltrona > (npoltrone-1))) {
						puts("ATTENZIONE: inserimento non valido (numero fila o poltrona non ammessi)");
						puts("Ritenta...");
						i--;  // in modo tale da ripete l'inserimento
						continue;
					}

					if (mappa[fila][poltrona] == POSTO_OCCUPATO) {
						puts("ATTENZIONE: Posto gia' occupato!");
						puts("Prova a sceglierne un'altro");
						i--; // in modo tale da ripete l'inserimento
						continue;
					}

					// inserito =  -1;
					for (ii = 0; ii < daprenotare; ii++){
						if ((fila == booking[ii][0]) && (poltrona == booking[ii][1])){
							inserito = 1;
							puts("ATTENZIONE: Posto gia' inserito!");
							puts("Prova a sceglierne un'altro");
							i--; // in modo tale da ripete l'inserimento
							break;
						}
					}

					if (inserito == -1){
						booking[i][0] = fila;
						booking[i][1] = poltrona;
						puts("Poltrona valida");
					}

				}
				
				system("clear");
				puts("Riepilogo poltrone inserite [n.fila|n.poltrona]:");
				for (i = 0; i < daprenotare; i++){
					printf("[%d|%d]\n", (booking[i][0]) + 1, (booking[i][1]) + 1);
				}
				puts("Inoltro la richiesta di prenotazione...");

				// invio richiesta GETBOOKING
				codIO = GETBOOKING;
				if (write(datasock, &codIO, 4) < 0) {  
					fprintf(stderr, "Errore invio GETBOOKING\n");
					exit(EXIT_FAILURE);
				}

				// invio numero posti da prenotare (dimensione booking)
				if (write(datasock, &daprenotare, 4) < 0) {
					fprintf(stderr, "Errore invio numero di posti da prenotare\n");
					exit(EXIT_FAILURE);
				}

				// invio mappa booking
				if((write(datasock, booking, sizeof(booking))) < 0){
					fprintf(stderr, "Errore invio poltrone da prenotare\n");
					exit(EXIT_FAILURE);
				}

				// lettura codice segreto prenotazione oppure FAILURE
				bzero(&codIO, 4);
				if((read(datasock, &codIO, 4) ) < 0){  // risposta del server = FAILURE o codice segreto
					fprintf(stderr, "Errore lettura risposta da parte del server\n");
					exit(EXIT_FAILURE);
				}
				if (codIO == FAILURE){
					puts("Errore da parte del server");
					exit(EXIT_SUCCESS);

				}

				system("clear");
				puts("Prenotazione avvenuta con successo!");
				printf("CODICE PRENOTAZIONE <%d>\n", codIO);
				puts("ATTENZIONE: questo codice va conservato per un eventuale disdetta");
				puts("Arrivederci e grazie per aver scelto il nostro cinema!");

				// invio messaggio EXIT al server
				codIO = EXIT;
				if (write(datasock, &codIO, 4) < 0) {  
					fprintf(stderr, "Errore invio EXIT\n");
					exit(EXIT_FAILURE);
				}

			} 

			break;  // fine case: GATMAPPA
		}


		case DISDETTA: {
			system("clear");
			puts("Inserisci il codice numerico relativo alla prenotazione");
			puts("[0] per tornare al menu");
			scanf("%d", &coddisdetta);

			if (coddisdetta == 0){  // torna al menu principale
				system("clear");
				break;
			} else {

				// invio richiesta DISDETTA
				codIO = DISDETTA;
				if (write(datasock, &codIO, 4) < 0) {
					fprintf(stderr, "Errore invio DISDETTA\n");
					exit(EXIT_FAILURE);
				}

				// invio codice prenotazione per disdetta
				if (write(datasock, &coddisdetta, 4) < 0) {
					fprintf(stderr, "Errore invio codice prenotazione per disdetta\n");
					exit(EXIT_FAILURE);
				}

				// leggo risposta
				bzero(&codIO, 4);
				if (read(datasock, &codIO, 4) < 0) {
					fprintf(stderr, "Errore lettura risposta del server\n");
					exit(EXIT_FAILURE);
				}

				system("clear");
				switch(codIO){
					case FAILURE: {
						puts("Errore da parte del server");
						exit(EXIT_SUCCESS);
					}
					case NOCOD: {
						puts("Spiacente codice non valido");
						break;
					}
					case SUCCESS: {
						puts("Disdetta avvenuta con successo!");
						puts("A presto...");
						break;
					}
				}

				// invio messaggio EXIT al server
				codIO = EXIT;
				if (write(datasock, &codIO, 4) < 0) {  
					fprintf(stderr, "Errore invio EXIT\n");
					exit(EXIT_FAILURE);
				}

			}

			break;
			
		}  // END CASE DISDETTA

		default:
			fprintf(stderr, "Errore invio Menu principale\n");
			exit(EXIT_FAILURE);
			break;
	}

	} while(codIO != EXIT); // END DO-WHILE

	close(datasock);

	exit(EXIT_SUCCESS);
}

int menu(){

	char scelta;

	puts("-------------------------------------");
	puts(" Scegli una tra le seguenti opzioni:");
	puts("-------------------------------------");
	puts(" [1] Prenotazione posti");
	puts(" [2] Disdetta prenotazione");
	puts(" [0] EXIT");
	puts("-------------------------------------");

	scanf(" %c", &scelta);  // <<< N.B.

	switch(scelta){
		case '0':
			return EXIT;
			break;
		case '1':
			return GETMAPPA;
			break;
		case '2':
			return DISDETTA;
			break;
		default:
			system("clear");
			puts("ATTENZIONE - Inserimento errato!");
			puts("Torno al menu principale...");
			menu();
			break;
	}

	return FAILURE;

}

